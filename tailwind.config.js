const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import("@types/tailwindcss/tailwind-config").TailwindConfig } */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        bebas: ['"Bebas"'],
      },
      colors: {
        "c-ink": "#1E4C7B",
        "c-main-blue": "#5F8BFF",
        "c-main-gray": "#9B9B9B",
        "c-gray-20": "#E0E0EA",
        "c-gray-30": "#C5C5CD",
        "c-gray-40": "#8A8A9D",
        "c-main-dark": "#3E3E41",
        "c-border-gray": "#C1C1C2",
        "c-light-gray": "#F5F5FB",
        "c-violet": "#31315B",
      },
      animation: {
        "spin-slow": "spin 2s linear infinite",
      },
    },
  },
  daisyui: {
    logs: false,
    darkTheme: false,
  },
  plugins: [
    require("daisyui"),
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography"),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/aspect-ratio"),
  ],
};

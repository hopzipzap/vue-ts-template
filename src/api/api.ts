import axios, {
  AxiosResponse,
  type AxiosError,
  type AxiosHeaders,
} from "axios";
import { useUserStore } from "@/store/user.store";

export const api = axios.create({
  baseURL: "http://localhost:3000/api",
  withCredentials: true,
});

const apiRequest = (
  method: string,
  url: string,
  request?: object,
  headers?: typeof AxiosHeaders,
): Promise<AxiosResponse<any, any>> => {
  return api({
    method,
    url,
    data: request,
    headers,
  })
    .then((res) => {
      console.log(res);
      return Promise.resolve(res);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};
const get = (url: string, request?: object) => apiRequest("get", url, request);

const deleteRequest = (url: string, request?: object) =>
  apiRequest("delete", url, request);

const post = (url: string, request: object, headers?:typeof AxiosHeaders) =>
  apiRequest("post", url, request, headers);

const put = (url: string, request: object) => apiRequest("put", url, request);

const patch = (url: string, request: object) =>
  apiRequest("patch", url, request);

api.interceptors.request.use(
  async (config) => {
    if (config.url == "/users/login") return config;
    const userStore = useUserStore();

    const token = userStore.user.accessToken;
    if (token) {
      config.headers = {
        ...config.headers,
        authorization: `${token}`,
      };
    }

    return config;
  },
  (error) => Promise.reject(error),
);

api.interceptors.response.use(
  (response) => response,
  async (error: AxiosError) => {
    const userStore = useUserStore();

    if (error.response?.status == 401 && !error.response.data.failed) {
      await userStore.refresh();
      return await api(error.config);
    }
    if (error.response?.status == 401 && error.response.data.failed) {
      userStore.logout();
    }
    return Promise.reject(error);
  },
);
export const API = {
  get,
  post,
  put,
  patch,
  delete: deleteRequest,
};

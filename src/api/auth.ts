import { IUser } from "@/types";
import { API as api } from "./api";
import { jwtDecode } from "jwt-decode";
import { AxiosError } from "axios";

export const login = async (
  username: string,
  password: string,
): Promise<IUser> => {
  try {
    const res = await api.post("/users/login", {
      username,
      password,
    });

    return {
      ...jwtDecode(res.data.accessToken),
      accessToken: res.data.accessToken,
    };
  } catch (error) {
    throw error;
  }
};

export const refresh = async (): Promise<IUser> => {
  try {
    const res = await api.get("/users/refresh");

    return {
      ...jwtDecode(res.data.accessToken),
      accessToken: res.data.accessToken,
    };
  } catch (error) {
    throw error;
  }
};

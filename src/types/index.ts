import { ISvg } from "@/constants/svgs";
import { RouteRecordRaw } from "vue-router";
export type ButtonType = "button" | "info" | "error";

export interface IBreadCrumb {
  title: string;
  path: string;
}

export interface IRoute extends Omit<RouteRecordRaw, "meta"> {
  meta?(_route?: any): {
    title: string;
    sidebarTitle?: string;
    breadcrumbs?: IBreadCrumb[];
    isMenuItem?: boolean;
    icon?: ISvg;
  };
}

export interface IUser {
  username: string;
  full_name: string;
  role: number;
  accessToken: string;
}

export interface IFilter {
  modelValue: string | number | undefined;
  placeholder: string;
  label: string;
  type:
    | "button"
    | "checkbox"
    | "color"
    | "date"
    | "datetime-local"
    | "email"
    | "file"
    | "hidden"
    | "image"
    | "month"
    | "number"
    | "password"
    | "radio"
    | "range"
    | "reset"
    | "search"
    | "submit"
    | "tel"
    | "text"
    | "time"
    | "url"
    | "week";
  disabled: boolean;
  required: boolean;
  step: number;
}

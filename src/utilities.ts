export const toMoney = function (n: string, signCount: number = 2) {
  return Number(Number(n).toFixed(signCount))
    .toString()
    .replace(/(\d)(?=(\d{3})+\.)/g, "$1 ");
};

export const downloadBlob = function (blob: Blob, name = "file.txt") {
  const blobUrl = URL.createObjectURL(blob);

  const link = document.createElement("a");
  link.href = blobUrl;
  link.download = name;

  document.body.appendChild(link);

  link.dispatchEvent(
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
      view: window,
    }),
  );

  document.body.removeChild(link);
};

export const mapSqlDate = (date: Date) => {
  const result: Date = new Date(date);
  result.setHours(result.getHours() + 3);
  return result.toISOString().slice(0, 10).replace("T", "") as unknown;
};

export const idGen = (() => {
  function* gen() {
    let i = 0;

    while (true) {
      yield i++;
    }
  }

  return gen();
})();

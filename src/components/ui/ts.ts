export enum VARIANTS {
  SUCCESS = "success",
  ERROR = "error",
  WARNING = "warning",
  INFO = "info",
  PRIMARY = "primary",
}

import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";
import * as ROUTES from "./routes";
import { useUserStore } from "@/store/user.store";

export const router = createRouter({
  history: createWebHistory(),
  routes: Object.values(ROUTES) as RouteRecordRaw[],
});

router.beforeEach((to, _from, next) => {
  const user = useUserStore().user;

  if (to.name !== "Auth") {
    if (!user) {
      return next({
        path: "/auth",
      });
    }
  } else if (user) {
    return next({ path: "/" });
  }

  return next();
});

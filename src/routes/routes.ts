/** @type {import('vue-router').RouterOptions['routes']} */

import { svgs } from "@/constants/svgs";
import { IRoute } from "@/types";

export const DEFAULT: IRoute = {
  path: "/",
  redirect: "/orders",
};

export const AUTH: IRoute = {
  name: "Auth",
  path: "/auth",
  component: () => import("@/pages/Auth.vue"),
  meta: (_route: any) => ({
    title: "Вход",
  }),
};

export const SERVER_ERROR: IRoute = {
  name: "ServerError",
  path: "/server_error",
  component: () => import("@/pages/ServerError.vue"),
  meta: (_route: any) => ({
    title: "Сервер недоступен",
  }),
};

// example route
// export const ORDER: IRoute = {
//   name: "Order",
//   path: "/orders/:orderId",
//   component: () => import("@/pages/OrderPage.vue"),
//   meta: (_route: any) => ({
//     sidebarTitle: "Заказы",
//     title: `Заказ №${_route?.query.id_year}`,
//     breadcrumbs: [
//       {
//         title: `Заказы`,
//         path: `/orders`,
//       },
//       {
//         title: `Заказ №${_route?.query.id_year}`,
//         path: `/orders/${_route?.params.orderId}`,
//       },
//     ],
//     isMenuItem: false,
//     icon: svgs.add,
//   }),
// };

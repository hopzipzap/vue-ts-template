import Decimal from "decimal.js";

export class Order {
  id: number | null = null;
  sub_type: string = "";
  thickness: string = "";
  fact_thickness: string = "";
  color: string = "";
  unique_id: string | null = null;
  status: number = 0;
  manager: string = "";
  note: string = "";
  client: string = "";
  operator: null = null;
  roll: null = null;
  payment: string = "";
  phonenumber: string = "";
  cost: number = 0;
  createdAt: Date | undefined = undefined;
  readyAt: Date = new Date();
  departAt: Date | null = null;
  productionAt: Date | null = null;
  finishAt: Date | null = null;
  comment: string = "";
  side: string = "";
  inprodmokr: number = 0;
  inprodbataysk: number = 0;
  id_year: string = "";
  idCounter: number | null = null;
  year: number = new Date().getFullYear();
  type: string = "";
  inner_thickness: string = "";
  inner_color: string = "";
  total_count: number = 0;
  total_cost: Decimal = new Decimal(0);
  total_sqr: Decimal = new Decimal(0);
  total_len: Decimal = new Decimal(0);
  rows: Rows = {
    proflist: [],
    other: [],
    sandwich: [],
    fence: [],
    pipe: [],
    pipeton: [],
    necond: [],
  };
}

export interface Rows {
  proflist: Proflist[];
  other: Other[];
  sandwich: Sandwich[];
  fence: Fence[];
  pipe: Pipe[];
  pipeton: Pipeton[];
  necond: Necond[];
}
export class Proflist {
  len: Decimal | undefined = undefined;
  count: number | undefined = undefined;
  sqr: Decimal | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}
export class Sandwich {
  sqr: Decimal | undefined = undefined;
  count: number | undefined = undefined;
  len: Decimal | undefined = undefined;
  costpm: Decimal | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}
export class Pipeton {
  type: string | undefined;
  count: number | undefined = undefined;
  tonn: Decimal | undefined = undefined;
  cost: Decimal | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}
export class Pipe {
  type: string | undefined;
  count: number | undefined = undefined;
  pm: Decimal | undefined = undefined;
  cost: Decimal | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}
export class Other {
  product: string | undefined;
  color: string | undefined;
  count: number | undefined = undefined;
  cost: Decimal | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}
export class Necond {
  type: string | undefined;
  pm: Decimal | undefined = undefined;
  count: number | undefined = undefined;
  cost: Decimal | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}

export class Fence {
  len: Decimal | undefined = undefined;
  fullLength: Decimal | undefined = undefined;
  count: number | undefined = undefined;
  fcost: Decimal | undefined = undefined;
  uniqid?: string;
  id?: number;
}

import { defineStore } from "pinia";
import { auth } from "@/api";
import { IUser } from "@/types";
import { ref } from "vue";
const win: Window = window;

export const useUserStore = defineStore(
  "user",
  () => {
    const user = ref<IUser>(null as unknown as IUser);

    const login = async (username: string, password: string) => {
      const res = await auth.login(username, password);
      user.value = res;
    };

    const logout = async () => {
      user.value = null as unknown as IUser;
      win.location = "/auth";
    };

    const refresh = async () => {
      const res = await auth.refresh();
      user.value = res;
    };

    return {
      user,
      login,
      logout,
      refresh,
    };
  },
  {
    persist: true,
  },
);

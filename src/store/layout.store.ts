import { defineStore } from "pinia";
import { Component, ref } from "vue";

export const useLayout = defineStore("layout", () => {
  const loading = ref<boolean>(false);
  const isPrint = ref<boolean>(false);
  const component = ref<Component | null>(null);

  const showPrint = (_component: Component) => {
    isPrint.value = true;
    component.value = _component;
  };

  const hidePrint = () => {
    isPrint.value = false;
    component.value = null;
  };
  return {
    component,
    isPrint,
    loading,
    showPrint,
    hidePrint,
  };
});

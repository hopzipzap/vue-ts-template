import { defineStore } from "pinia";
import { ref } from "vue";

export enum NotificationType {
  Info = "info",
  Success = "success",
  Warning = "warning",
  Error = "error",
}
export interface INotification {
  message: string;
  type: NotificationType;
  notifyTime?: number;
}

export const useNotifications = defineStore("notification", () => {
  const notifications = ref<INotification[]>([]);

  const notify = (notification: INotification) => {
    notification.notifyTime = Date.now();
    notifications.value.push(notification);
    console.log(notifications.value);
    setTimeout(removeNotification, 2000, notification);
  };

  const removeNotification = (notification: INotification) => {
    notifications.value = notifications.value.filter(
      (n) => n.notifyTime !== notification.notifyTime,
    );
  };

  return {
    notifications,
    notify,
  };
});

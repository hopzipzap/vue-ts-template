import { markRaw } from "vue";
import { defineStore } from "pinia";

export type Modal = {
  isOpen: boolean;
  view: object;
  actions?: ModalAction[];
  properties?: object[];
};

export type ModalAction = {
  label: string;
  callback: (props?: unknown) => void;
};

export const useModal = defineStore("modal", {
  state: (): Modal => ({
    isOpen: false,
    view: {},
    actions: [],
    properties: [],
  }),
  actions: {
    open(view: object, actions?: ModalAction[], properties?: object[]) {
      this.isOpen = true;
      this.actions = actions;
      this.properties = properties;
      // using markRaw to avoid over performance as reactive is not required
      this.view = markRaw(view);
    },
    close() {
      this.isOpen = false;
      this.view = {};
      this.actions = [];
      this.properties = [];
    },
  },
});

export default useModal;

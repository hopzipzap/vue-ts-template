declare module "*.vue" {
  import type { DefineComponent } from "vue";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const component: DefineComponent<object, object, any>;
  export default component;
}

declare module "bun" {
  interface Env {
    API_URL: string;
    PROJECT_TITLE_RU: string;
    PROJECT_TITLE_EN: string;
  }
}

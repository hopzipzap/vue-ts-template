import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import * as path from "path";
import tailwindcss from "tailwindcss";

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  server: {
    open: true,
  },
});
